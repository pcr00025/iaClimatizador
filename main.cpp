#include <Arduino.h>
#include <OneWire.h>                
#include <DallasTemperature.h>
#include <LCD_I2C.h>
#include <Servo.h>

LCD_I2C lcd(0x27);
OneWire ourWire(50);
DallasTemperature sensor(&ourWire);
Servo servoMotor;
int bombilla = 40;
int ventilador = 30;
int servo = 34;
int ledRojo = 10;
int ledAmbar = 9;
int ledVerde = 8;
bool abierto = false;

float medirTemp() {
  sensor.requestTemperatures();
  return sensor.getTempCByIndex(0);
}

void calentar(int intensidad) {
  analogWrite(bombilla, intensidad);
}

void abre(){
  digitalWrite(ledRojo, LOW);
  digitalWrite(ledAmbar, HIGH);
  for (int i = 9; i <= 110; i++) {
    servoMotor.write(i);
    delay(30);
  }
  digitalWrite(ledAmbar, LOW);
  abierto = true;
}

void cierra(){
  digitalWrite(ledVerde, LOW);
  digitalWrite(ledAmbar, HIGH);
  for (int i = 109; i >= 8; i--) {
    servoMotor.write(i);
    delay(30);
  }
  digitalWrite(ledAmbar, LOW);
  abierto = false;
}

void setup() {
  Serial.begin(9600);
  Serial3.begin(9600);  // Transmisión a ESP8266
  pinMode(ledRojo, OUTPUT);
  pinMode(ledAmbar, OUTPUT);
  pinMode(ledVerde, OUTPUT);
  pinMode(ventilador, OUTPUT);
  servoMotor.attach(servo);
  servoMotor.write(8);  // Ángulo inicial
  sensor.begin();
  sensor.setResolution(12);
  lcd.begin();
  lcd.backlight();
}

void loop() {
  float temp = medirTemp();
  lcd.print(" Temp: ");
  lcd.print(temp);
  lcd.print((char)223);
  lcd.print("C");
  Serial3.write((int)temp);
  if (temp >= 25) {
    calentar(0);
    digitalWrite(ventilador, HIGH);
    if (!abierto) {
      abre();
    }
    digitalWrite(ledVerde, HIGH);
    digitalWrite(ledRojo, LOW);
  }
  if (temp <= 24) {
    digitalWrite(ventilador, LOW);
    if (abierto) {
      cierra();
    }
    digitalWrite(ledRojo, HIGH);
    digitalWrite(ledVerde, LOW);
    calentar(150);
  }
  delay(3000);
  lcd.clear();  
}