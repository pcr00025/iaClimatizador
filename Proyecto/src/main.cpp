#include <Arduino.h>
#include <OneWire.h>                
#include <DallasTemperature.h>
#include <LCD_I2C.h>
#include <Servo.h>

LCD_I2C lcd(0x27);
OneWire ourWire(50);
DallasTemperature sensor(&ourWire);
Servo servoMotor;
int resistencia = 44;
int peltier = 38;
int servo = 32;
int pinSubir = 7;
int pinBajar = 6;
bool abierto = false;
float tempInicial = 26.0;
float tempObj = tempInicial;
float limSup = tempObj + 0.5;
float limInf = tempObj - 0.5;
bool enfriando = false;
float temp;
int ledV = 11;
int ledA = 12;
int ledR = 13;

float medirTemp() {
  sensor.requestTemperatures();
  return sensor.getTempCByIndex(0);
}

void calentar(int intensidad) {
  analogWrite(resistencia, intensidad);
}

void enfriar(int intensidad) {
  analogWrite(peltier, intensidad);
}

void abre() {
  if (!abierto) {
    for (int i = 9; i <= 110; i++) {
      servoMotor.write(i);
      delay(30);
    }
    abierto = true;
  }
}

void cierra() {
  if (abierto) {
    for (int i = 109; i >= 8; i--) {
      servoMotor.write(i);
      delay(30);
    }
    abierto = false;
  }
}

void visualizacion(float tempActual, float tempObjetivo) {
  Serial.print(tempActual);
  Serial.print(" - ");
  Serial.println(tempObjetivo);
  // Temperatura sensor
  lcd.print("Temp: ");
  lcd.print(tempActual);
  lcd.print((char)223);
  lcd.print("C");
  // Temperatura objetivo
  lcd.setCursor(0,1);
  lcd.print("Objetivo: ");
  lcd.print(tempObjetivo);
  lcd.print((char)223);
  lcd.print("C");
}

void visualizacionTEST() {
  // Temperatura sensor
  lcd.print("Temp: ");
  lcd.print(temp);
  // Temperatura objetivo
  lcd.setCursor(0,1);
  lcd.print(limInf);
  lcd.print("-");
  lcd.print(tempObj);
  lcd.print("-");
  lcd.print(limSup);
}

float modTempObj() {
  float t = tempObj;
  if (digitalRead(pinSubir) == HIGH) {
    //Serial.println("SUBIR");
    if (tempObj < 31)
      t = tempObj + 0.5;
  }
  if (digitalRead(pinBajar) == HIGH) {
    //Serial.println("BAJAR");
    if (tempObj > 25)
    t = tempObj - 0.5;
  }
  return t;
}

void semaforo() {
  if (abierto) {
    digitalWrite(ledA, HIGH);
  }
  else {
    digitalWrite(ledA, LOW);
  }
  if (enfriando) {
    digitalWrite(ledV, HIGH);
    digitalWrite(ledR, LOW);
  }
  else {
    digitalWrite(ledV, LOW);
    digitalWrite(ledR, HIGH);
  }
}

void setup() {
  Serial.begin(9600);
  sensor.begin();
  sensor.setResolution(12);
  lcd.begin();
  lcd.backlight();
  servoMotor.attach(servo);
  servoMotor.write(8);  // Ángulo inicial
  calentar(0);
  enfriar(0);
  pinMode(pinBajar, INPUT);
  pinMode(pinSubir, INPUT);
  pinMode(ledR, OUTPUT);
  pinMode(ledA, OUTPUT);
  pinMode(ledV, OUTPUT);
}

void loop() {
  semaforo();
  temp = medirTemp();
  //tempObj = tempInicial + 10*analogRead(potenciometro)/1023/2.0;
  tempObj = modTempObj();
  limSup = tempObj + 0.5;
  limInf = tempObj - 0.5;
  visualizacion(temp, tempObj);
  //visualizacionTEST();

  // CALENTAR
  if (!enfriando) {
    if (temp >= limInf) {
      //Serial.println("CALENTAR: temp >= limInf");
      calentar(0);
      if (temp >= tempObj) {
        //Serial.println("CALENTAR: temp >= tempObj");
        abre();
        if (temp >= limSup) {
          //Serial.println("CALENTAR: temp >= limSup");
          enfriar(255);
          enfriando = true;
        }
      }
    }
    else {
      //Serial.println("CALENTAR: calentar(255)");
      calentar(255);
    }
  }
  // ENFRIAR
  else {
    if (temp <= tempObj) {
      //Serial.println("ENFRIAR: temp <= tempObj");
      enfriar(0);
      cierra();
      if (temp <= limInf-0.5) {
        //Serial.println("ENFRIAR: temp <= limInf");
        calentar(255);
        enfriando = false;
      }
    }
    else {
      //Serial.println("ENFRIAR: enfriar(255)");
      enfriar(255);
    }
  }
  delay(1000);
  lcd.clear();
}